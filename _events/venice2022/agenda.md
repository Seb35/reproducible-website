---
layout: event_detail
title: Agenda
event: venice2022
order: 10
permalink: /events/venice2022/agenda/
---

Reproducible Builds Summit 2022 - Working [Agenda]({{ "/events/venice2022/agenda/" | relative_url }})

The following was the working schedule for the 2022 Reproducible Builds Summit. 

Day 1 - Tuesday 1 November
--------------------------

* 9.00 Opening Circle
* 9.30 Project updates
  * Round I
	* Warpforge
		* LINK(S)
	* Eclipse Adoption Reproducible OpenJDK Builds
		* LINK(S)
	* Tails vs squashfs-tools
		* LINK(S)
	* Snapshot Server
		* LINK(S)
  * Round II
	* Major update in IT regulation/license compliance & regulation compliance
		* LINK(S)
	* IT security research, RB adoption in supply chains
		* Landing page of our study: [https://research.teamusec.de/2022-interviews-reproducible/](https://research.teamusec.de/2022-interviews-reproducible/])
	* Package rebuilder and debrebuild
		* LINK(S)
	* RB for Maven / RB in Maven Central Repository
		* RB for Maven: [https://maven.apache.org/guides/mini/guide-reproducible-builds.html](https://maven.apache.org/guides/mini/guide-reproducible-builds.html
)
		* RB in Maven Central Repository: [https://github.com/jvm-repo-rebuild/reproducible-central](https://github.com/jvm-repo-rebuild/reproducible-central)
  * Round IIIa
	* GNU Guix
		* LINK(S)
	* NetBSD
		* LINK(S)
	* Status of R-B Debian
		* LINK(S)
	* Tor Browser - Going to release 12.0 (a major update)
		* LINK(S)
	* OpenWRT
		* LINK(S)

* 10.45 Break - All break times are approximate :^)
* 11.00 Mapping the state of Reproducible Builds 
* 12.30 Lunch
* 13.30 Collaborative Working Sessions
  * Documentation FIXME https://pad.riseup.net/p/rbsummmit2022-documentation-keep
  * Tools FIXME https://pad.riseup.net/p/rbsummmit2022-tools-keep
  * Packaging FIXME https://pad.riseup.net/p/rbsummmit2022-packaging-keep
* 15.00 Break - All break times are approximate :^)
* 15.30 Closing Circle
* 15.45  Hacking Time
  * first stub for a read-the-doc conversion
    * [online rendering](https://abate.gitlab.io/reproducible-build.org/)
    * [git repository](https://gitlab.com/abate/reproducible-build.org)
* 17.00 Adjourn

Day 2 - Wednesday 2 November
----------------------------

* 9.00 Opening Circle
  * The day will start with a summary of Day 1 outcomes and a Day 2 Agenda Overview.
* 9.30 Collaborative Working Sessions, break-out discussions continue.
  * Documentation Tooling FIXME https://pad.riseup.net/p/rbsummmit2022-doc-tooling-keep
  * Metrics FIXME https://pad.riseup.net/p/rbsummmit2022-metrics-keep
  * Packaging FIXME https://pad.riseup.net/p/rbsummmit2022-packagingII-keep
  * Motivation FIXME https://pad.riseup.net/p/rbsummmit2022-motivation-keep
* 11.15 Break - All break times are approximate :^)
* 11.15 Participant Skill Share
  * Participants will be encouraged to share any skill the consider relevant to the meeting scope. The session will be structured so as to minimize group size and maximize 1-on-1 sharing opportunities.
* 12.30 Lunch
* 13.30 Collaborative Working Sessions
  * Source Mirrors FIXME https://pad.riseup.net/p/rbsummmit2022-src-mirror-keep
  * SBOM FIXME https://pad.riseup.net/p/rbsummmit2022-sbom-keep
  * Packaging III FIXME https://pad.riseup.net/p/rbsummmit2022-packagingIII-keep
  * Motivation II FIXME https://pad.riseup.net/p/rbsummmit2022-motivationII-keep
* 15.00 Break - All break times are approximate :^)
* 15:15 Hacking Time
* 16.45 Closing Circle
* 17.00 Adjourn

Day 3 - Thursday 3 November
---------------------------

* 9.00 Opening Circle
  * The day will start with a summary of Day 2 outcomes and a Day 3 Agenda Overview.
* 9.30 Collaborative Working Sessions, break-out discussions continue.
  * Verifying packages at installation discussion (in-toto): FIXME https://pad.riseup.net/p/rbsummmit2022-installation-keep
  * RB Taxonomy FIXME https://pad.riseup.net/p/rbsummmit2022-taxonomy-keep
  * Debian FIXME https://pad.riseup.net/p/rbsummmit2022-debian-keep
  * Firmware FIXME https://pad.riseup.net/p/rbsummmit2022-firmware-keep
* 10.45 Break - All break times are approximate :^)
* 11.00 Collaborative Working Sessions, break-out discussions continue.
  * in-toto vs sbom (spdx) FIXME https://pad.riseup.net/p/rbsummmit2022-intoto-vs-sbom
* 12.30 Lunch
* 13.30 Mapping Where From Here
  * The group will pause before the final session to take stock of the progress made to this point in the week and to inventory action items, next steps and other bridges to post-event collaboration. FIXME https://pad.riseup.net/p/rbsummmit2022-i-will-we-should-keep
* 16.15 Closing Circle
  * Participants will summarize key outcomes from the event, and discuss next steps for continuing collaboration after the meeting.
* 17.00 Adjourn



