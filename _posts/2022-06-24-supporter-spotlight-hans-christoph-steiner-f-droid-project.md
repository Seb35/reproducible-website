---
layout: post
title: "Supporter spotlight: Hans-Christoph Steiner of the F-Droid project"
date: 2022-06-24 10:00:00
categories: org
---

[![]({{ "/images/news/supporter-spotlight-hans/fdroid.jpg?#right" | relative_url }})](https://f-droid.org/)

<big>The Reproducible Builds project [relies on several projects, supporters and sponsors]({{ "/who/" | relative_url }}) for financial support, but they are also valued as ambassadors who spread the word about our project and the work that we do.</big>

This is the *fifth* instalment in a series featuring the projects, companies and individuals who support the Reproducible Builds project. We started this series by [featuring the Civil Infrastructure Platform]({{ "/news/2020/10/21/supporter-spotlight-cip-project/" | relative_url }}) project and followed this up with a [post about the Ford Foundation]({{ "/news/2021/04/06/supporter-spotlight-ford-foundation/" | relative_url }}) as well as a recent ones about [ARDC]({{ "/news/2022/04/14/supporter-spotlight-ardc/" | relative_url }}), the [Google Open Source Security Team (GOSST)]({{ "/news/2022/04/26/supporter-spotlight-google-open-source-security-team/" | relative_url }}) and [ Jan Nieuwenhuizen on Bootstrappable Builds, GNU Mes and GNU Guix]({{ "/news/2022/05/18/jan-nieuwenhuizen-on-bootrappable-builds-gnu-mes-and-gnu-guix/" | relative_url }}).

Today, however, we will be talking with <big>**Hans-Christoph Steiner** from the **F-Droid** project</big>.

<br>
<br>

[![]({{ "/images/news/supporter-spotlight-hans/hans.jpg?#right" | relative_url }})](https://at.or.at/hans/)

**Chris Lamb: Welcome, Hans! Could you briefly tell me about yourself?**

Hans: Sure. I spend most of my time trying to private communications software usable by everyone, designing interactive software with a focus on human perceptual capabilities and building networks with free software. I've been involved in Debian since approximately 2008 and became an official Debian developer in 2011. In the little time left over from that, I sometimes compose music with computers from my home in Austria.

<br>

**Chris: For those who have not heard of it before, what is the F-Droid project?**

Hans: F-Droid is a community-run app store that provides free software applications for Android phones. First and foremost our goal is to represent our users. In particular, we review all of the apps that we distribute, and these reviews not only check for whether something is definitely free software or not, we additionally look for 'ethical' problems with applications as well — issues that we call 'Anti-Features'. Since the project began in 2010, F-Droid now offers almost 4,000 free-software applications.

F-Droid is also a 'app store kit' as well, providing all the tools that are needed to operate an free app store of your own. It also includes complete build and release tools for managing the process of turning app source code into published builds.

<br>

**Chris: How exactly does F-Droid differ from the Google Play store? Why might someone use F-Droid over Google Play?**

![]({{ "/images/news/supporter-spotlight-hans/screenshot.png?#right" | relative_url }})

Hans: One key difference to the Google Play Store is that F-Droid does not ship proprietary software by default. All apps shipped from `f-droid.org` are built from source on our own builders. This is partly because F-Droid is backed by the free software community; that is, people who have engaged in the free software community long before Android was conceived, and, in particular, share many — if not all — of its values. Using F-Droid will therefore feel very familiar to anyone familiar with a modern Linux distribution.

<br>

**Chris: How do you describe reproducibility from the F-Droid point of view?**

Hans: All centralised software repositories are extremely tempting targets for exploitation by malicious third parties, and the kinds of personal or otherwise sensitive data on our phones only increase this risk. In F-Droid's case, not only could the software we distribute be theoretically replaced with nefarious copies, the build infrastructure that generates that software could be compromised as well.

F-Droid having reproducible builds is extremely important as it allows us to verify that our build systems have not been compromised and distributing malware to our users. In particular, if an independent build infrastructure can produce precisely the same results from a build, then we can be reasonably sure that they haven't been compromised. Technically-minded users can also validate their builds on their own systems too, further increasing trust in our build infrastructure. (This can be performed using `fdroid verify` command.)

Our signature & trust scheme means that F-Droid can verify that an app is 100% free software whilst still using the developer's original `.APK` file. More details about this may be found in our [reproducibility documentation](https://f-droid.org/en/docs/Reproducible_Builds/) and on the page about our [Verification Server](https://f-droid.org/docs/Verification_Server/).

<br>

**Chris: How do you see F-Droid fitting into the rest of the modern security ecosystem?**

[![]({{ "/images/news/supporter-spotlight-hans/antifeatures.png?#right" | relative_url }})](https://f-droid.org/en/docs/Anti-Features/)

Hans: Whilst F-Droid inherits all of the social benefits of free software, F-Droid takes special care to respect your privacy as well — we don't attempt to track your device in any way. In particular, you don't need an account to use the F-Droid client, and F-Droid doesn't send any device-identifying information to our servers... other than its own version number.

What is more, we mark all apps in our repository that track you, and users can choose to hide any apps that has been tagged with a specific [Anti-Feature](https://f-droid.org/en/docs/Anti-Features/) in the F-Droid settings. Furthermore, any personal data you decide to give us (such as your email address when registering for a forum account) goes no further than us as well, and we pledge that it will never be used for anything beyond merely maintaining your account.

<br>

**Chris: What would 'fully reproducible' mean to F-Droid? What it would look like if reproducibility was a 'solved problem'? Or, rather, what would be your 'ultimate' reproducibility goals?**

Hans: In an ideal world, every application submitted to F-Droid would not only build reproducibly, but would come with a cryptographically-signed signature from the developer as well. Then we would only distribute an compiled application after a build had received a number of matching signatures from multiple, independent third parties. This would mean that our users were not placing their trust solely in software developers' machines, and wouldn't be trusting our centralised build servers as well.

<br>

**Chris: What are the biggest blockers to reaching this state? Are there any key steps or milestones to get there?**

[![]({{ "/images/news/supporter-spotlight-hans/publish-chart.png?#right" | relative_url }})](https://f-droid.org/en/docs/Reproducible_Builds/)

Hans: Time is probably the main constraint to reaching this goal. Not only do we need system administrators on an ongoing basis but we also need to incorporate reproducibly checks into our Continuous Integration (CI) system. We are always looking for new developers to join our effort, as well as learning about how to better bring them up to speed.

Separate to this, we often experience blockers with reproducibility-related bugs in the Android build tooling. Luckily, upstreams do ultimately address these issues, but in some cases this has taken three or four years to reach end-users and developers. Unfortunately, upstream is not chiefly concerned with the *security* aspects of reproducibility builds; they care more about how it can minimise and optimise download size and speed.

<br>

**Chris: Are you tracking any statistics about reproducibility in F-Droid over time? If so, can you share them? Does F-Droid track statistics about its own usage?**

Hans: To underline a topic touched on above, F-Droid is dedicated to preserving the privacy of its users; we therefore do not  record usage statistics. This is, of course, in contrast to other application stores.

However, we *are* in a position to track whether packages in F-Droid are reproducible or not. As in: what proportion of APKs in F-Droid have been independently verified over time? Unfortunately, due to time constraints, we do not yet automatically publish charts for this.


We do publish some raw data that is related, though, and we naturally welcome contributions of visualizations based on any and all of our data. The "[All our APIs](https://f-droid.org/docs/All_our_APIs/)" page on our wiki is a good place to start for someone wanting to contribute, everything about reproducible F-Droid apps is available in JSON format, what's missing are apps or dashboards making use of the available raw data.

<br>

**Chris: Many thanks for this interview, Hans, and for all of your work on F-Droid and elsewhere. If someone wanted to get in touch or learn more about the project, where might someone go?**

[![]({{ "/images/news/supporter-spotlight-hans/fdroid.jpg?#right" | relative_url }})](https://f-droid.org/)

Hans: The best way to find out about F-Droid is, of course, the [main F-Droid homepage](https://f-droid.org/), but we are also on Twitter [@fdroidorg](https://twitter.com/fdroidorg). We have many avenues to participate and to learn more! We have an [About](https://f-droid.org/en/about/) page on our website and a [thriving forum](https://forum.f-droid.org/). We also have part of our documentation [specifically dedicated to reproducible builds](https://f-droid.org/en/docs/Reproducible_Builds/).

<br>

---

<br>
*For more information about the Reproducible Builds project, please see our website at
[reproducible-builds.org]({{ "/" | relative_url }}). If you are interested in
ensuring the ongoing security of the software that underpins our civilisation
and wish to sponsor the Reproducible Builds project, please reach out to the
project by emailing
[contact@reproducible-builds.org](mailto:contact@reproducible-builds.org).*
