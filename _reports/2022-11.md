---
layout: report
year: "2022"
month: "11"
title: "Reproducible Builds in November 2022"
draft: false
date: 2022-12-08 17:45:11
---

[![]({{ "/images/reports/2022-11/reproducible-builds.png#right" | relative_url }})](https://reproducible-builds.org/)

Welcome to yet another report from the [Reproducible Builds](https://reproducible-builds.org) project, this time for *November 2022*. In all of these reports (which we have been [publishing regularly since May 2015]({{ "/news/" | relative_url }})) we attempt to outline the most important things that we have been up to over the past month. As always, if you interested in contributing to the project, please visit our [*Contribute*]({{ "/contribute/" | relative_url }}) page on our website.

<br>

### Reproducible Builds Summit 2022

[![]({{ "/images/reports/2022-11/summit_photo.jpg#right" | relative_url }})]({{ "/events/venice2022/" | relative_url }})

Following-up from [last month's report]({{ "/reports/2022-10/" | relative_url }}) about our recent summit in Venice, Italy, a comprehensive report from the meeting has not been finalised yet — watch this space!

As a very small preview, however, we can link to several issues that were filed about the website during the summit ([#38](https://salsa.debian.org/reproducible-builds/reproducible-website/-/issues/38), [#39](https://salsa.debian.org/reproducible-builds/reproducible-website/-/issues/39), [#40](https://salsa.debian.org/reproducible-builds/reproducible-website/-/issues/40),  [#41](https://salsa.debian.org/reproducible-builds/reproducible-website/-/issues/41), [#42](https://salsa.debian.org/reproducible-builds/reproducible-website/-/issues/42), [#43](https://salsa.debian.org/reproducible-builds/reproducible-website/-/issues/43), etc.) and collectively learned about [Software Bill of Materials](https://en.wikipedia.org/wiki/Software_supply_chain#Usage) (SBOM)'s and how `.buildinfo` files can be seen/used as SBOMs. And, no less importantly, the Reproducible Builds t-shirt design has been updated…

---

### Reproducible Builds at European Cyber Week 2022

[![]({{ "/images/reports/2022-11/cyberweek.jpg#right" | relative_url }})](https://www.european-cyber-week.eu/?lang=en)

During the [European Cyber Week 2022](https://www.european-cyber-week.eu/?lang=en), a [Capture The Flag](https://en.wikipedia.org/wiki/Capture_the_flag_(cybersecurity)) (CTF) cybersecurity challenge was created by Frédéric Pierret on the subject of Reproducible Builds. The challenge consisted in a pedagogical sense based on how to make a software release reproducible. To progress through the challenge issues that affect the reproducibility of build (such as build path, timestamps, file ordering, etc.) were to be fixed in steps in order to get the final 'flag' in order to win the challenge.

At the end of the competition, five people succeeded in solving the challenge, all of whom were awarded with a shirt. Frédéric Pierret intends to create similar challenge in the form of a "how to" in the [Reproducible Builds documentation]({{ "/docs/" | relative_url }}), but two of the 2022 winners are shown here:

[![]({{ "/images/reports/2022-11/IMG_20221130_220704_288.jpg#center" | relative_url }})](https://www.european-cyber-week.eu/?lang=en)

---

### ‘*On business adoption and use of reproducible builds…*’

[![]({{ "/images/reports/2022-11/butler.png#right" | relative_url }})](https://link.springer.com/article/10.1007/s11219-022-09607-z)

Simon Butler [announced on the *rb-general* mailing list](https://lists.reproducible-builds.org/pipermail/rb-general/2022-November/002761.html) that the [Software Quality Journal](https://www.springer.com/journal/11219) published an article called [*On business adoption and use of reproducible builds for open and closed source software*](https://link.springer.com/article/10.1007/s11219-022-09607-z).

This article is an interview-based study which focuses on the adoption and uses of Reproducible Builds in industry, with a focus on investigating the reasons why organisations might not have adopted them:

> […] industry application of R-Bs appears limited, and we seek to understand whether awareness is low or if significant technical and business reasons prevent wider adoption.

This is achieved through interviews with software practitioners and business managers, and touches on both the business and technical reasons supporting the adoption (or not) of Reproducible Builds. The article also begins with an excellent explanation and literature review, and even introduces a new helpful analogy for reproducible builds:

> [Users are] able to perform a bitwise comparison of the two binaries to verify that they are identical and that the distributed binary is indeed built from the source code in the way the provider claims. Applied in this manner, R-Bs **function as a canary**, a mechanism that indicates when something might be wrong, and offer an improvement in security over running unverified binaries on computer systems.

The [full paper](https://link.springer.com/article/10.1007/s11219-022-09607-z) is available to download on an '[open access](https://en.wikipedia.org/wiki/Open_access)' basis.

[![]({{ "/images/reports/2022-11/integrity-paper.png#right" | relative_url }})](https://arxiv.org/pdf/2211.06249.pdf)

Elsewhere in academia, Beatriz Michelson Reichert and Rafael R. Obelheiro have published a paper proposing a systematic threat model for a generic software development pipeline identifying possible mitigations for each threat ([PDF](https://arxiv.org/pdf/2211.06249.pdf)). Under the *Tampering* rubric of their paper, various attacks against Continuous Integration (CI) processes:

> An attacker may insert a backdoor into a CI or build tool and thus introduce vulnerabilities into the software (resulting in an improper build). To avoid this threat, it is the developer’s responsibility to take due care when making use of third-party build tools. Tampered compilers can be mitigated using diversity, as in the diverse double compiling (DDC) technique. **Reproducible builds, a recent research topic, can also provide mitigation for this problem.** ([PDF](https://arxiv.org/pdf/2211.06249.pdf))

---

### Misc news

[![]({{ "/images/reports/2022-11/golang.png#right" | relative_url }})](https://go.dev/)

* A change was proposed for the [Go programming language](https://go.dev/) to [enable reproducible builds when Link Time Optimisation (LTO) is enabled](https://go-review.googlesource.com/c/go/+/413974). As mentioned in the changelog, Morten Linderud's patch fixes two issues when the linker used in conjunction with the `-flto` option: the first involves solving an issue related to [seeded random numbers](https://en.wikipedia.org/wiki/Random_seed); and the second involved the binary embedding the current working directory in compressed sections of the LTO object. Both of these issues made the build unreproducible.

* In the [.NET framework ecosystem](https://en.wikipedia.org/wiki/.NET), a wiki page for the [Roslyn .NET C# and Visual Basic compiler](https://learn.microsoft.com/en-gb/dotnet/csharp/roslyn-sdk/) was uncovered this month that [details its attempts to ensure end-to-end reproducible builds](https://github.com/dotnet/roslyn/blob/main/docs/compilers/Deterministic%20Inputs.md) by focusing on the definition on what are 'considered inputs to the compiler for the purpose of determinism'. This is a spiritual followup to a 2016 blog post by Microsoft developer [Jared Parsons](https://blog.paranoidcoding.com/) on '[Deterministic builds in Roslyn](https://blog.paranoidcoding.com/2016/04/05/deterministic-builds-in-roslyn.html)' which starts: 'It seems silly to celebrate features which should have been there from the start.'

* [Ian Lance Taylor followed up an old post](https://gcc.gnu.org/pipermail/gcc-patches/2022-November/606205.html) to report that Jakub Jelinek's patch from September 2000 is incomplete.

[![]({{ "/images/reports/2022-11/fdroid.png#right" | relative_url }})](https://f-droid.org/)

* In [F-Droid](https://f-droid.org/) this month, Reproducible Builds contributor FC Stegerman created a [set of 'reproducible APK tools'](https://github.com/obfusk/reproducible-apk-tools) as a workaround for issues like [the order of files in APKs built on macOS being non-deterministic](https://gitlab.com/fdroid/fdroiddata/-/issues/2816#note_1179533719). In addition, the new issue documenting [the overview of apps using reproducible builds](https://gitlab.com/fdroid/fdroiddata/-/issues/2844) shows that F-Droid added 11 new apps that use reproducible builds, and FC Stegerman released *apksigcopier* version 1.1.0 which [adds support for APKs signed by 'Signflinger'](https://github.com/obfusk/apksigcopier#what-about-apks-signed-by-gradlezipflingersignflinger-instead-of-apksigner).

* *martinSusz* has written up a [fascinating wiki page](https://github.com/martinSusz/rkdeveloptool/wiki/Generating--quasi-reproducible-BootROM-firmware-for-Rock-Chips-SoC) describing how to generate 'quasi-reproducible' firmware ROMs for [System-on-a-Chip](https://en.wikipedia.org/wiki/System_on_a_chip) (SoC) components fabricated by [Rock Chip](https://en.wikipedia.org/wiki/Rockchip). These chips are used in popular low-cost laptops such as the [Pine64 PinebookPro](https://www.pine64.org/pinebook-pro/) and [Asus C201](https://www.asus.com/us/laptops/for-home/chromebook/asus-chromebook-c201/). The link is worth viewing simply for the [interesting diagram](https://user-images.githubusercontent.com/119517241/205492847-f2e03cd0-c7b4-43f2-b7f0-e970e531e805.png).

* Our monthly IRC meeting was [held on November 29th 2022](http://meetbot.debian.net/reproducible-builds/2022/reproducible-builds.2022-11-29-14.58.html). Our next meeting will be on **January 31st 2023**; we'll skip the meeting in December due to the proximity to Christmas, etc.

---

On [our mailing list](https://lists.reproducible-builds.org/listinfo/rb-general/) this month:

* Adrian Diglio from Microsoft asked "[How to Add a New Project within Reproducible Builds](https://lists.reproducible-builds.org/pipermail/rb-general/2022-November/002726.html)" which solicited a number of replies.

* Vagrant Cascadian posed an interesting question regarding the difference between "test builds" vs "rebuilds" (or "verification rebuilds"). As [Vagrant poses in their message](https://lists.reproducible-builds.org/pipermail/rb-general/2022-November/002747.html), "they're both useful for slightly different purposes, and it might be good to clarify the distinction […]."

---

### Debian & other Linux distributions

[![]({{ "/images/reports/2022-11/debian.png#right" | relative_url }})](https://debian.org/)

Over 50 reviews of Debian packages were added this month, another 48 were updated and almost 30 were removed, all of which adds to [our knowledge about identified issues](https://tests.reproducible-builds.org/debian/index_issues.html). Two new issue types were added as well.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/9c673bfa)][[...](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/bbe35ed0)].

Vagrant Cascadian announced on [our mailing list](https://lists.reproducible-builds.org/pipermail/rb-general/) another online sprint to help 'clear the huge backlog of reproducible builds patches submitted' by performing NMUs ([Non-Maintainer Uploads](https://wiki.debian.org/NonMaintainerUpload)). The first such sprint took place on September 22nd, but others were held on October 6th and October 20th. There were two additional sprints that occurred in November, however, which resulted in the following progress:

* Chris Lamb:

    * [`paxctl`](https://tracker.debian.org/pkg/paxctl) (Fixed [#1020804](https://bugs.debian.org/1020804))
    * [`png23d`](https://tracker.debian.org/pkg/png23d) (Fixed [#1020805](https://bugs.debian.org/1020805))
    * [`tuxcmd-modules`](https://tracker.debian.org/pkg/tuxcmd-modules) (Fixed [#1011500](https://bugs.debian.org/1011500) & [#941296](https://bugs.debian.org/941296))
    * [`waili`](https://tracker.debian.org/pkg/waili) (Fixed [#1020751](https://bugs.debian.org/1020751))
    * [`zephyr`](https://tracker.debian.org/pkg/zephyr) (Fixed [#828867](https://bugs.debian.org/828867) [#1021374](https://bugs.debian.org/1021374))

* Vagrant Cascadian:

    * [`ddd`](https://tracker.debian.org/pkg/ddd) (Fixed [#834016](https://bugs.debian.org/834016))
    * [`libpam-ldap`](https://tracker.debian.org/pkg/libpam-ldap) (Fixed [#834050](https://bugs.debian.org/834050))
    * [`nsnake`](https://tracker.debian.org/pkg/nsnake/) (Fixed [#833612](https://bugs.debian.org/833612))
    * [`quvi`](https://tracker.debian.org/pkg/quvi) (Fixed [#835259](https://bugs.debian.org/835259))
    * [`stressapptest`](https://tracker.debian.org/pkg/stressapptest) (Fixed [#831587](https://bugs.debian.org/831587) & [#986653](https://bugs.debian.org/986653))
    * [`tcpreen`](https://tracker.debian.org/pkg/tcpreen) (Fixed [#831585](https://bugs.debian.org/831585))
    * [`boolector`](https://tracker.debian.org/pkg/boolector) (Fixed [#1023886](https://bugs.debian.org/1023886))
    * [`tsdecrypt`](https://tracker.debian.org/pkg/tsdecrypt) (Fixed [#829713](https://bugs.debian.org/829713) & [#1022130](https://bugs.debian.org/1022130))
    * [`wbxml2`](https://tracker.debian.org/pkg/wbxml2) (QA upload fixed build path issues)
    * [`tercpp`](https://tracker.debian.org/pkg/tercpp) (QA upload fixed build path issues)

Lastly, Roland Clobus posted his [latest update of the status of reproducible Debian ISO images](https://lists.reproducible-builds.org/pipermail/rb-general/2022-November/002760.html) on our mailing list. This reports that 'all major desktops build reproducibly with *bullseye*, *bookworm* and *sid*' as well as that no custom patches needed to applied to Debian *unstable* for this result to occur. During November, however, Roland [proposed some modifications to *live-setup*](https://salsa.debian.org/images-team/live-setup/-/merge_requests/2) and the rebuild script has been adjusted to fix the failing Jenkins tests for Debian *bullseye* [[...](https://salsa.debian.org/live-team/live-build/-/merge_requests/293)][[...](https://salsa.debian.org/qa/jenkins.debian.net/-/commit/e237cab3ed87826b1c25b068b88fabcecf020d21)].

<br>

In other news, [Miro Hrončok](https://fedoraproject.org/wiki/User:Churchyard) proposed a change to 'clamp' build modification times to the value of [`SOURCE_DATE_EPOCH`]({{ "/docs/source-date-epoch/" | relative_url }}). This was [initially suggested and discussed on a `devel@` mailing list post](https://lists.fedoraproject.org/archives/list/devel@lists.fedoraproject.org/thread/MWKWFO52KTOGVGOEUDZT7YBOON2G5A2K/) but was later [written up on the Fedora Wiki](https://fedoraproject.org/wiki/Changes/ReproducibleBuildsClampMtimes) as well as being [officially proposed to Fedora Engineering Steering Committee (FESCo)](https://pagure.io/fesco/issue/2899).

---

### Upstream patches

The Reproducible Builds project detects, dissects and attempts to fix as many currently-unreproducible packages as possible. We endeavour to send all of our patches upstream where appropriate. This month, we wrote a large number of such patches, including:

* Bernhard M. Wiedemann:

    * [`dwz`](https://build.opensuse.org/request/show/1036953) ([Profile-guided optimisation](https://en.wikipedia.org/wiki/Profile-guided_optimization) issue)
    * [`icmake`](https://gitlab.com/fbb-git/icmake/-/merge_requests/5) (filesystem ordering issue)
    * [`llmnrd`](https://build.opensuse.org/request/show/1037221)
    * [`elixir`](https://bugzilla.opensuse.org/show_bug.cgi?id=1205134) (report a bug re. stuck build on single-core VMs)
    * [`warzone2100`](https://github.com/Warzone2100/warzone2100/issues/2991) (report a bug re. parallelism-dependent output)

* Chris Lamb:

    * [#1023589](https://bugs.debian.org/1023589) filed against [`libnvme`](https://tracker.debian.org/pkg/libnvme).
    * [#1024352](https://bugs.debian.org/1024352) filed against [`pykafka`](https://tracker.debian.org/pkg/pykafka).

* Vagrant Cascadian:

    * [#1023886](https://bugs.debian.org/1023886) filed against [`boolector`](https://tracker.debian.org/pkg/boolector).
    * [#1023956](https://bugs.debian.org/1023956) filed against [`fl-cow`](https://tracker.debian.org/pkg/fl-cow).
    * [#1023957](https://bugs.debian.org/1023957) filed against [`gerstensaft`](https://tracker.debian.org/pkg/gerstensaft).
    * [#1023960](https://bugs.debian.org/1023960) filed against [`libcgicc`](https://tracker.debian.org/pkg/libcgicc).
    * [#1024007](https://bugs.debian.org/1024007) filed against [`haskell98-report`](https://tracker.debian.org/pkg/haskell98-report).
    * [#1024125](https://bugs.debian.org/1024125) filed against [`ucspi-proxy`](https://tracker.debian.org/pkg/ucspi-proxy).
    * [#1024126](https://bugs.debian.org/1024126) filed against [`hunt`](https://tracker.debian.org/pkg/hunt).
    * [#1024279](https://bugs.debian.org/1024279) filed against [`tolua++`](https://tracker.debian.org/pkg/tolua++).
    * [#1024282](https://bugs.debian.org/1024282) filed against [`twoftpd`](https://tracker.debian.org/pkg/twoftpd).
    * [#1024283](https://bugs.debian.org/1024283) filed against [`ipsvd`](https://tracker.debian.org/pkg/ipsvd).
    * [#1024284](https://bugs.debian.org/1024284) filed against [`gentoo`](https://tracker.debian.org/pkg/gentoo).
    * [#1024286](https://bugs.debian.org/1024286) filed against [`lcm`](https://tracker.debian.org/pkg/lcm).
    * [#1024288](https://bugs.debian.org/1024288) filed against [`apcupsd`](https://tracker.debian.org/pkg/apcupsd).
    * [#1024289](https://bugs.debian.org/1024289) filed against [`openfortivpn`](https://tracker.debian.org/pkg/openfortivpn).
    * [#1024290](https://bugs.debian.org/1024290) filed against [`xtb`](https://tracker.debian.org/pkg/xtb).
    * [#1024291](https://bugs.debian.org/1024291) filed against [`gnunet`](https://tracker.debian.org/pkg/gnunet).
    * [#1024292](https://bugs.debian.org/1024292) filed against [`swift-im`](https://tracker.debian.org/pkg/swift-im).
    * [#1024396](https://bugs.debian.org/1024396) filed against [`brewtarget`](https://tracker.debian.org/pkg/brewtarget).
    * [#1024399](https://bugs.debian.org/1024399) filed against [`xrprof`](https://tracker.debian.org/pkg/xrprof).
    * [#1024404](https://bugs.debian.org/1024404) filed against [`gitlint`](https://tracker.debian.org/pkg/gitlint).
    * [#1024412](https://bugs.debian.org/1024412) filed against [`claws-mail`](https://tracker.debian.org/pkg/claws-mail).
    * [#1024413](https://bugs.debian.org/1024413) filed against [`presage`](https://tracker.debian.org/pkg/presage).
    * [#1024530](https://bugs.debian.org/1024530) filed against [`jh7100-bootloader-recovery`](https://tracker.debian.org/pkg/jh7100-bootloader-recovery).

* Victor Westerhuis:

    * [#1024482](https://bugs.debian.org/1024482) & [#1024638](https://bugs.debian.org/1024638) filed against [`opencv`](https://tracker.debian.org/pkg/opencv).

* John Neffenger:

    * [`tomcat`](https://github.com/apache/tomcat/pull/566) (Fixed Apache bug [#66346](https://bz.apache.org/bugzilla/show_bug.cgi?id=66346))

---

### [diffoscope](https://diffoscope.org)

[![]({{ "/images/reports/2022-11/diffoscope.png#right" | relative_url }})](https://diffoscope.org)

[*diffoscope*](https://diffoscope.org) is our in-depth and content-aware diff utility. Not only can it locate and diagnose reproducibility issues, it can provide human-readable diffs from many kinds of binary formats. This month, Chris Lamb prepared and uploaded versions `226` and `227` to Debian:

* Support both `python3-progressbar` and `python3-progressbar2`, two modules providing the `progressbar` Python module. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/81903e0b)]
* Don't run Python decompiling tests on Python bytecode that `file(1)` cannot detect yet and Python 3.11 cannot unmarshal. ([#1024335](https://bugs.debian.org/1024335))
* Don't attempt to attach text-only differences notice if there are no differences to begin with. ([#1024171](https://bugs.debian.org/1024171))
* Make sure we recommend `apksigcopier`. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/792115b9)]
* Tidy generation of `os_list`. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/f0be250e)]
* Make the code clearer around generating the Debian 'substvars'. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/58fd63c8)]
* Use our `assert_diff` helper in `test_lzip.py`. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/6d3a2779)]
* Drop other copyright notices from `lzip.py` and `test_lzip.py`. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/4a6f0811)]

In addition to this, Christopher Baines added [*lzip*](https://www.nongnu.org/lzip/) support&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/e1b947b8)], and FC Stegerman added an optimisation whereby we don't run `apktool` if no differences are detected before the signing block&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/1852890a)].

---

[![]({{ "/images/reports/2022-11/reproducible-builds.png#right" | relative_url }})](https://reproducible-builds.org/)

A significant number of changes were made to the Reproducible Builds website and documentation this month, including Chris Lamb ensuring the [openEuler](https://www.openeuler.org/en/) logo is correctly visible with a white background&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/f0251baa)], FC Stegerman de-duplicated by email address to avoid listing some contributors twice&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/fed37547)], Hervé Boutemy added [Apache Maven](https://maven.apache.org/) to the [list of affiliated projects]({{ "/who/projects/" | relative_url }})&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/5879bed0)] and *boyska* updated our [*Contribute*]({{ "/contribute/" | relative_url }}) page to remark that the [Reproducible Builds presence on *salsa.debian.org*](https://salsa.debian.org/reproducible-builds/) is not just the Git repository but is also for creating issues&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/5471f8d8)][[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/9b4238ad)]. In addition to all this, however, Holger Levsen made the following changes:

* Add a number of existing publications&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/6a3972fa)][[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/82d4570c)] and update metadata for some existing publications as well&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/435e36ec)].
* Hide draft posts on the [website homepage]({{ "/" | relative_url }}).&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/684acdce)]
* Add the Warpforge build tool as a participating project of the summit.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/6f076241)]
* Clarify in the footer that we welcome patches to the website repository.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/7ffc9492)]

---

### Testing framework

[![]({{ "/images/reports/2022-11/testframework.png#right" | relative_url }})](https://tests.reproducible-builds.org/)

The Reproducible Builds project operates a comprehensive testing framework at [tests.reproducible-builds.org](https://tests.reproducible-builds.org) in order to check packages and other artifacts for reproducibility. In October, the following changes were made by Holger Levsen:

* Improve the generation of 'meta' package sets (used in grouping packages for reporting/statistical purposes) to treat Debian *bookworm* as equivalent to Debian *unstable* in this specific case&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/3d3ab211)]
and to parse the list of packages used in the Debian cloud images&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/7f65008c)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/a62656fa)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/5b079c49)].
* Temporarily allow Frederic to `ssh(1)` into our snapshot server as the `jenkins` user.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/9f407d14)]
* Keep some reproducible jobs Jenkins logs much longer&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/65fd1455)] ([later reverted](https://salsa.debian.org/qa/jenkins.debian.net/commit/7101f5c9)).
* Improve the node health checks to detect failures to update the Debian cloud image package set&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/c4d670d2)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/7223af42)] and to improve prioritisation of some kernel warnings&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/2f1cf2e6)].
* Always echo any IRC output to Jenkins' output as well.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/6589dc83)]
* Deal gracefully with problems related to processing the cloud image package set.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/997b8184)]

Finally, Roland Clobus continued his work on testing Live Debian images, including adding support for specifying the origin of the Debian installer&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/e237cab3)] and to warn when the image has unmet dependencies in the package list (e.g. due to a transition)&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/a9a96757)].

<br>

If you are interested in contributing to the Reproducible Builds project, please visit our [*Contribute*](https://reproducible-builds.org/contribute/) page on our website. You can get in touch with us via:

 * IRC: `#reproducible-builds` on `irc.oftc.net`.

 * Twitter: [@ReproBuilds](https://twitter.com/ReproBuilds)

 * Mailing list: [`rb-general@lists.reproducible-builds.org`](https://lists.reproducible-builds.org/listinfo/rb-general)
