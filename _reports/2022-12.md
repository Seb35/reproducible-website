---
layout: report
year: "2022"
month: "12"
title: "Reproducible Builds in December 2022"
draft: true
---

Debian NMU sprint 2022-12-01:

* Vagrant Cascadian

  squeak-plugins-scratch NMU https://bugs.debian.org/876771 & https://bugs.debian.org/942006
  stgit NMU https://bugs.debian.org/942009
  desmume NMU DELAYED/10 https://bugs.debian.org/890312
  strace Investigated https://bugs.debian.org/896016
  apophenia Closed, no longer applicable https://bugs.debian.org/940013
  chessx Closed, no longer applicable https://bugs.debian.org/881664
  netgen-lvs  NMU DELAYED/10 https://bugs.debian.org/955783
  libstatgrab NMU DELAYED/10 https://bugs.debian.org/961747
  xavs2 confirmed patch https://bugs.debian.org/952493
  intel-gpu-tools submitted alternate patch https://bugs.debian.org/945105

  NMU Sprint 2022-12-08 https://pad.sfconservancy.org/p/0beva37rttcy4oZksHMu

  NMU Sprint 2022-12-15

* Vagrant Cascadian

  libjama NMU DELAYED/10 https://bugs.debian.org/986601
  liblip NMU DELAYED/10 https://bugs.debian.org/1001513 and https://bugs.debian.org/989583
  mlpost Closed, no longer applicable https://bugs.debian.org/977179 and https://bugs.debian.org/977180
  wxmaxima Closed, fixed in new upstream version https://bugs.debian.org/983148

  NMU Sprint 2022-12-22

* Vagrant Cascadian

  ario NMU DELAYED/10 https://bugs.debian.org/828876
  choqok Closed, no longer needed, commented onbuild path issues https://bugs.debian.org/825322
  cloop NMU (no delay) https://bugs.debian.org/787996 and https://bugs.debian.org/1005414 and https://bugs.debian.org/1005413
  clhep: NMU DELAYED/10 https://bugs.debian.org/794398

  NMU Sprint 2022-12-29

* Holger Levsen

  * apr-util NMU DELAYED/10 https://bugs.debian.org/1006865
  * lirc worked on better fix for https://bugs.debian.org/979024 though this should really be addressed upstream as layed out in https://github.com/pypa/setuptools/issues/2133
  * no change source NMU of ruby-omniauth-tumblr as a very old upload from 2016 (=built on a buildd, but before dpkg supported .buildinfo files) now remigrated to testing.

* Vagrant Cascadian

  * pytsk NMU https://bugs.debian.org/992060
  * cmocka NMU https://bugs.debian.org/991181
  * xaw3d NMU DELAYED/10 https://bugs.debian.org/991180 and https://bugs.debian.org/986704
  * cfi NMU DELAYED/10 https://bugs.debian.org/995647
  * surgescript NMU DELAYED/10 https://bugs.debian.org/992061
  * perfect-scrollbar NMU DELAYED/10 https://bugs.debian.org/1000770
  * smplayer marked as done, no longer affected https://bugs.debian.org/997689
  * python-tomli marked as done, no longer affected https://bugs.debian.org/994979

* [FIXME](https://gist.github.com/obfusk/c51ebbf571e04ddf29e21146096675f8)

* [FIXME](https://github.com/mastodon/mastodon-android/issues/4#issuecomment-1336259343)

* [FIXME: APK ordering differences were not caused by building on macOS after all, but by using Android Studio](https://gitlab.com/fdroid/fdroiddata/-/issues/2816#note_1204147286)

* [FIXME](https://pad.sfconservancy.org/p/0beva37rttcy4oZksHMu)

* FIXME: Foxboron suspects if https://github.com/golang/go/issues/57120
  is implemented "properly"" then the distributed Go binaries from Google and Arch Linux might be identical.
  Foxboron: "It's just a bit early to sorta figure out what roadblocks there are. Go bootstraips itself every build. So in theory I think it should be possible".

* FIXME: Holger interviewed [David A. Wheeler on supply chain security](https://reproducible-builds.org/news/2022/12/15/supporter-spotlight-davidawheeler-supply-chain-security/) in our supporter spotlight series.

* [forwarded 1025801](https://github.com/sphinx-doc/sphinx/pull/11037)

* [FIXME](https://remy.grunblatt.org/nix-and-nixos-my-pain-points.html)

* FIXME: [Reproducible Builds Summit 2023 in Hamburg, Germany](https://reproducible-builds.org/events/hamburg2023/)
  * When:  October 30th, November 1st-2nd 2023.
  * What:  Three days to continue the grow of the Reproducible Builds effort. As previously, the exact content of the meeting will be shaped by the participants.
  * Where: Hamburg, Germany at https://dock-europe.net/

* [FIXME: F-Droid added 16 new apps published with Reproducible Builds](https://gitlab.com/fdroid/fdroiddata/-/issues/2844)
